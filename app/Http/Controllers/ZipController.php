<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;
use Response;
use Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
class ZipController extends Controller
{
    // Global varial within the controller
    private $_file = array(), 
            $_zip = null;
    
    public function __construct(){
        // declarring class gloabbly because I use this morethan once.
        $this->_zip = new ZipArchive;
    }
    
    public function index(){
        // Zip Main View
        return view('zip.index');
    }

    public function upload(Request $request){

        // validating the file as required before the process continue
        $rules = array (
            'upload_file' => 'required',
        );
        $validator = Validator::make ( Input::all (), $rules );
        // if fail return error message
        if ($validator->fails ()) {
            return Redirect::back()->withErrors($validator);
        }else{
            //checking file if has
            if ($request->hasfile('upload_file')) {
                // save the file first before compress and uncompress
                $filename = $request->file('upload_file')->getClientOriginalName();
                $request->upload_file->move(public_path('file/'), $filename);

                // validating file if already compressed so file will be
                if ($request->upload_file->getClientOriginalExtension() == 'zip') {
                    if ($this->_zip->open(public_path('file/'.$request->file('upload_file')->getClientOriginalName())) === TRUE) {
                        $this->_zip->extractTo(public_path('file'));
                        $this->_zip->close();
                    }

                }else{
                    // if file not zip extension it will be compress to zip file
                    $this->add('file/'.$request->file('upload_file')->getClientOriginalName());
                    $this->store('file/'.basename($request->file('upload_file')->getClientOriginalName(), '.'.$request->file('upload_file')->getClientOriginalExtension()).'.zip');
                    $file= public_path('file/'.basename($request->file('upload_file')->getClientOriginalName(), '.'.$request->file('upload_file')->getClientOriginalExtension()).'.zip');
                    $headers = array(
                            'Content-Type: application/zip',
                            );

                            // dowloading the file after compress
                    return  Response::download($file, basename($request->file('upload_file')->getClientOriginalName(), '.'.$request->file('upload_file')->getClientOriginalExtension()).'.zip', $headers);
                }
            }
            
        }
    }

    // collecting the file and location
    public function add($input){
           $this->_file = array_merge($this->_file, $input);
    }
    // compress and  store the file to the folder
    public function store($location = null){
        if (count($this->_file) && $location) {
            foreach($this->_file as $index => $file){
                if (!file_exists($file)) {
                    unset($this->_file[$index]);
                }
            }
        }
        if ($this->_zip->open($location, file_exists($location) ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE)) {
            foreach ($this->_file as $file) {
                $this->_zip->addFile($file,$file);
            }
            $this->_zip->close();
        }
    }
}
