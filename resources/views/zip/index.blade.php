@extends('welcome')

@section('content')
    <div class="container">
        <form enctype="multipart/form-data" action="{{ route('upload') }}" method="post" >
        @csrf
            <div class="form-group">
                <label for="exampleFormControlFile1">Input File to Compress:</label>
                <input type="file" class="form-control-file" name="upload_file" id="upload_file" target="_blank">
                @if ($errors->has('upload_file'))
                    <div class="error">{{ $errors->first('upload_file') }}</div>
                @endif
            </div>
            <button type="submit">Submit</button>
        </form>
    </div>

@endsection