@extends('welcome')

@section('content')
    <div class="container">
 
            <div class="form-group">
            @for ($i = 1; $i <= 1000; $i++) 
                @if ($i % 15 == 0) 
                <span class="text-danger">{{ $i.'-FizzBuzz'}}</span>,
                @elseif ($i % 3 == 0) 
                <span class="text-info"> {{ $i.'-Fizz'}}</span>,
                @elseif ($i % 5 == 0) 
                <span class="text-success">{{ $i.'-Buzz'}}</span>,
                @else 
                    {{ $i }},
                @endif
            @endfor
              
            </div>
    </div>

@endsection