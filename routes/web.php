<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/zip', 'ZipController@index');
Route::post('zip/upload',  'ZipController@upload')->name('upload');

Route::get('/fizzbazz ', 'FizzBazzController@index');